function setFormMessage(el, data){
    if(data.hasOwnProperty('stat') && data.hasOwnProperty('msg')){
        $(el).fadeIn(200, function(){ $(el).html(((data.stat)? '<div class="alert alert-success rounded-0">'+data.msg+'</div>':'<div class="alert alert-danger rounded-0">'+data.msg+'</div>'));});
		setTimeout(function(){$(el).fadeOut(1000, function(){$(el).html('');});}, 2000);
    }
	if(data.hasOwnProperty('csrf')){
        $('meta[name='+data.csrf.name+']').attr("content", data.csrf.token);
    }
    if(data.hasOwnProperty('msgField')){
        $.each(data.msgField, function(i,v){
            if(!$('#'+i).hasClass('is-invalid')) $('#'+i).addClass('is-invalid');
            if(!$('#'+i).next('div').hasClass('is-invalid'))$('#'+i).after('<div id="'+i+'-error" class="invalid-feedback">'+v+'</div>');
        });
    }
    if(data.hasOwnProperty('rowDel')){
        $(data.rowDel).remove();
    }
}

function refreshToken(data){
    if(data.hasOwnProperty('csrf')){
        $('meta[name='+data.csrf.name+']').attr("content", data.csrf.token);
    }
}

function setActiveMenu(menu, submenu1 = '', submenu2=''){
	menu = menu.trim(); submenu1 = submenu1.trim(); submenu2 = submenu2.trim();
    $( "li a.nav-link" ).each(function(){
        $(this).removeClass('active');
        if($(this).parent().hasClass('has-treeview')) $(this).parent().removeClass('menu-open');
    });
    if(menu.length > 0){
		if($('.'+menu).parent().hasClass('has-treeview')){$('.'+menu).parent().addClass('menu-open')}
		$('.'+menu).addClass('active');
	}
    if(submenu1.length > 0){ 
		if($('.'+submenu1).parent().hasClass('has-treeview')){
			$('.'+submenu1).parent().addClass('menu-open');
		}else{
			$('.'+submenu1).addClass('active');
		}
	}
    if(submenu2.length > 0) $('.'+submenu2).addClass('active');
}

function blockUI(blc, t = 1, s){
	let msg = '<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div>';
	if (t == 'progress') {
		msg = '<h4 class="progress-tittle">Silahkan tunggu...</h4><div class="progress progress-status"><div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 1%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">1%</div></div>';
	}
	blc = (typeof blc == 'undefined')? 'body' : blc;
    $(blc).block({
        onOverlayClick: $.unblockUI,
        overlayCSS: {
            backgroundColor: '#8EA0D7',
            cursor: 'default'
        },
        message: msg,
        css: {
            border: 'none',
            color: '#4D79FF',
            background: 'none',
            cursor: 'default',
        },
        baseZ: 1050
    });
	
	if(typeof s != 'undefined'){
		progressUnblockUI(blc, s, 0, 1);
	}
}

function unblockUI(blc){
    blc = (typeof blc == 'undefined')? 'body' : blc;
	$(blc).unblock();
}

// s: second, ps: progress second, ac: auto close
function progressUnblockUI(blc, s = 1, ps = 0, ac = 1){
	let p = Math.round(100/s)
	if(ps != s){
		ps++
		setTimeout(function(){
			let x = p * ps
			x = (x > 100)? '100%' : x+'%'
			$('.progress-bar').css('width', x).text(x);
			progressUnblockUI(blc, s, ps, ac)
		}, 1000);
	} else {
		if(ac) setTimeout(function(){unblockUI(blc)}, 1000);
	}
}

function closeModal(mdl, data){
    if(data.hasOwnProperty('mcc')){
        if(data.mcc) {
            setTimeout(function(){
                $modalConfirm.modal('hide');
            }, 1000);
        }
    } else if(data.hasOwnProperty('mc')){
        if(data.mc) {
            setTimeout(function(){
                $modal.modal('hide');
            }, 1000);   
        }
    }
}

function resetForm(el, exc){
    exc = (typeof exc != 'undefined')? exc : '';
    $('.select2', el).not(exc).val("").trigger("change");
    $(':input', el).not(':button, :submit, :reset, :radio'+((exc.length > 0)? ','+exc : '')).val('').prop('selected', false);
    $('label.custom-file-label').text('');
}

function setSandStr(target, fr, sep, ls) {
    if($(target).val().trim().length == 0){
        let r = '';
        let c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        if(typeof fr == 'number'){
            for ( let i = 0; i < fr; i++ ) r += c.charAt(Math.floor(Math.random() * 26));
        }else{
            r += fr;
        }
        r += sep;
        for ( let i = 0; i < ls; i++ ) r += c.charAt(Math.floor(Math.random() * c.length));
        $(target).val(r)
    }
 }

function getSandStr(fr, sep, ls) {
    let r = '';
    let c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < fr; i++ ) r += c.charAt(Math.floor(Math.random() * 26));
    r += sep;
    for (let i = 0; i < ls; i++ ) r += c.charAt(Math.floor(Math.random() * c.length));
    return r;
}

function setTransNo(target, pre, sep, suf){
    if($(target).val().trim().length == 0){
        let c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        pre += sep + c.charAt((moment().format('YY') - 1) % 36);
        pre += c.charAt((moment().format('M') - 1))+c.charAt((moment().format('D') - 1)) + sep;
        for ( let i = 0; i < suf; i++ ) pre += c.charAt(Math.floor(Math.random() * c.length));
        $(target).val(pre);
    }
}

function getInitialName(str = ''){
    if(str.length > 4){
        str = str.split(/[\s-.,&]+/)
        let pre = str[0].substr(0,3).toUpperCase()
        str.shift()
        return (str.length > 0)? pre+'-'+str.map((s) => s[0]).join('').toUpperCase() : pre;
    }else{
        return str.toUpperCase();
    }
}

function setInitialName(target = '', str = ''){
    $(target).val(getInitialName(str));
}

function ajaxChained(val, target, url, tname = '', adts = ''){
    var opt = '<option value="">- pilih -</option>';
    var c = 0;
    if(val.length > 0) {
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {[tname]: $('meta[name=' + tname + ']').attr("content"), id: val}
        }).done(function (data){
            refreshToken(data)
            let adt = adts.split(',');
            $.each(data.data, function (i, item) {
                let l = adt.length;
                opt += '<option value="' + item.id + '" ';
                for (let i = 0; i < l; i++) {
                    opt += 'data-' + adt[i] + '="' + item[adt[i]] + '" '
                }
                opt += '>' + item.text + '</option>';
            });
            c = data.data.length;
            $(target).html(opt);
            $(target).select2();

        });
    }else{
        $(target).html(opt);
        $(target).select2({placeholder: c+' results'});
    }
}

function getDayName(i){
    i = parseInt(i);
    let hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
    return (i>0 && i<8)? hari[i-1] : 'Kode hari salah. Rentang (1-7).';
}

function formatMoney(a){
    let s = parseInt(a).toString(), i  = s.length % 3, rp = s.substr(0, i), r  = s.substr(i).match(/\d{3}/gi);
    if(r){let n = i ? '.' : ''; rp += n + r.join('.');}
    return rp;
}

$('.date_filter').daterangepicker({
	startDate: moment().startOf('month'),
	endDate: moment(),
	locale:{format: 'DD-MM-YYYY', separator: ' ~ '},
	ranges: {
	   'Sekarang': [moment(), moment()],
	   'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	   'Tgl. 1 - Sekarang': [moment().startOf('month'), moment()],
	   '1 Minggu lalu': [moment().subtract(6, 'days'), moment()],
	   '30 Hari lalu': [moment().subtract(29, 'days'), moment()],
	   'Bulan ini': [moment().startOf('month'), moment().endOf('month')]
	}
});

$('.date_filter').on('cancel.daterangepicker', function(ev, picker) {
  $(this).val('');
});

$('.date_filter').on('apply.daterangepicker', function(ev, picker) {
	if(typeof dataTable != 'undefined'){ 
		blockUI('.card');
		dataTable.draw();
		setTimeout(function(){unblockUI('.card')}, 500);
	}
});

var $modal = $('#ajax-modal');
var $modalConfirm = $('#ajax-modal-confirm');
$('body').on('click', '.ajax_modal', function(ev) {
    ev.preventDefault();
    let u = $(this).data('url');
    let b = $(this).data('block');
    blockUI(b);
    setTimeout(function(){
        $modal.load(u, function(){
            $modal.modal('show');
        });
        unblockUI(b);
    }, 100);
});
$('body').on('click', '.ajax_modal_confirm', function(ev) {
    ev.preventDefault();
    let u = $(this).data('url');
    let b = $(this).data('block');
    blockUI(b);
    setTimeout(function(){
        $modalConfirm.load(u, function(){
            $modalConfirm.modal('show');
        });
        unblockUI(b);
    }, 100);
});

var hl= function (element, errorClass, validClass) {
    $(element).removeClass('is-valid').addClass('is-invalid');
    let elem = $(element);
    if (elem.hasClass("select2-offscreen")) {
        $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
    }
}
var uhl= function (element, errorClass, validClass) {
    $(element).removeClass('is-invalid');
    let elem = $(element);
    if (elem.hasClass("select2-offscreen")) {
        $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
    }
}
var sc= function (label, element) {
    label.remove();
    $(element).removeClass('is-invalid');
}
var erp= function (error, element) { // render error placement for each input type
    if(element.is("select") && (element.hasClass("select2") || element.hasClass("select2-hidden-accessible"))){
        $(element).parent().append(error);
    } else if (element.is("select") || element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file"|| element.attr("type") == "textarea") { // for chosen elements, need to insert the error after the chosen container
        error.insertAfter($(element).closest('.form-group').children('div').children().last());
    } else if (element.is("select")){
        error.appendTo($(element).parent());
    } else if (element.hasClass("ckeditor")) {
        error.appendTo($(element).closest('.form-group'));
    } else {
        error.insertAfter(element);
    }
}

var currencyMask = {prefix: "",autoUnmask: true, radixPoint: ",", groupSeparator: ".", digits: 0, autoGroup: true, allowMinus: false, min: 0, prefix: '', rightAlign: true, positionCaretOnClick: true,  digitsOptional: true, oncleared: function (self) { $(this).val('0'); }}
var discountMask = {prefix: "",autoUnmask: true, radixPoint: ",", groupSeparator: ".", digits: 2, autoGroup: true, allowMinus: false, min: 0, max: 99, prefix: '', rightAlign: true, positionCaretOnClick: true, digitsOptional: true, oncleared: function (self) { $(this).val('0'); }}
var debtMask = {prefix: "",autoUnmask: true, radixPoint: ",", groupSeparator: ".", digits: 0, autoGroup: true, allowMinus: true, prefix: '', rightAlign: true, positionCaretOnClick: true, digitsOptional: true, oncleared: function (self) { $(this).val('0'); }}
var datepickModal = {parentEl: ".modal-body",singleDatePicker: true, timePicker: true,timePicker24Hour: true, timePickerIncrement: 1, autoUpdateInput: true, locale:{format: 'YYYY-MM-DD HH:mm:00'}/*, startDate: moment().format('YYYY-MM-DD HH:mm:00')*/}
var daterangepickModal = {startDate: moment().startOf('month'),endDate: moment(),locale:{format: 'DD-MM-YYYY', separator: ' ~ '},ranges: {'Sekarang': [moment(), moment()],'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],'Tgl. 1 - Sekarang': [moment().startOf('month'), moment()],'1 Minggu lalu': [moment().subtract(6, 'days'), moment()],'30 Hari lalu': [moment().subtract(29, 'days'), moment()],'Bulan ini': [moment().startOf('month'), moment().endOf('month')],'Bulan Depan': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]}}
var chartOption = {responsive: true, maintainAspectRatio: false, datasetFill: false, legend: {display: false}, scaleStartValue: 0, scales: {yAxes: [{display: true, ticks: {suggestedMin: 0, beginAtZero: true}}]}, tooltips: {
                   callbacks: {label: function(tooltipItem, data) {return "Total: " +parseInt(tooltipItem.yLabel).toLocaleString('id-ID')+" "+data.datasets[tooltipItem.datasetIndex].label;}}}}
if(typeof $.ui != 'undefined'){$.ui.dialog.prototype._allowInteraction = function(e) {return !!$(e.target).closest('.ui-dialog, .ui-datepicker, .select2-drop').length}}
