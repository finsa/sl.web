<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Template_model extends MY_Model {

	public function get_posts($int_posts_type = 0, $limit = 0, $offset = 0, $order = 'dt_publish', $sort = 'DESC'){
        $this->db->select("posts.*, img.txt_dir")
        ->from($this->t_posts." posts")
        ->join($this->t_posts_img." img", "posts.int_posts_id = img.int_posts_id", "left")
        ->where('img.is_cover', 1);

        if($int_posts_type!=0){
            $this->db->where('int_posts_type', ($int_posts_type));
        }
        $this->db->limit($limit, $offset);
        return $this->db->order_by($order, $sort)->get()->result();
    }

    public function get_jadwal($txt_slug){
        $this->db->select("pages.*, img.txt_dir")
        ->from($this->t_pages." pages")
        ->join($this->t_pages_img." img", "pages.int_pages_id = img.int_pages_id", "left")
        ->where('pages.txt_slug', $txt_slug);

        return $this->db->get()->result();
    }

}