<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller
{
    public $autoload = array();
    public $http_status = 0;
    protected $breadcrumb;
    protected $page;
    protected $css;
    protected $js;
    protected $js_data;
    protected $module;
    protected $kodeMenu;
    protected $routeURL;
    protected $userAccess;
    protected $socketGroup;
    protected $use_minify = false;
    protected $minify_level = 1;
    protected $use_cache  = false;
    protected $hari = ['Day', 'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    protected $cache_time = 0;
    private $tokenName = '';
    private $tokenHash = '';

    public function __construct()
    {
        //x_debug($this->uri->uri_string());
        $class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
        //log_message('debug', $class." MX_Controller Initialized");
        Modules::$registry[strtolower($class)] = $this;

        /* copy a loader instance and initialize */
        $this->load = clone load_class('Loader');
        $this->load->initialize($this);

        /* autoload module items */
        $this->load->_autoloader($this->autoload);

        $this->tokenName = $this->security->get_csrf_token_name();
        $this->tokenHash = $this->security->get_csrf_hash();

        $this->page = (object) ['title' => CI::$APP->config->item('page_title'),
            'name'  => CI::$APP->config->item('app_name'),
            'alias' => CI::$APP->config->item('app_alias'),
            'cdns'  => CI::$APP->config->item('use_cdns'),
            'menu'  => '',
            'submenu1' => '',
            'submenu2' => '',
            'tokenName' => $this->tokenName];
        $this->breadcrumb = (object) ['title' => 'Dashboard',
            'list'  => ['Dashboard','Home']];

        $this->module       = 'homepage';
        //$this->socketGroup  = (object) ['warehouse' => mt_rand(0, 9999), 'conveyor' => mt_rand(9999, 19999)];
        $this->use_cache    = CI::$APP->config->item('use_cache');
        $this->cache_time   = CI::$APP->config->item('cache_time');
        $this->use_minify   = CI::$APP->config->item('use_minify');
        $this->minify_level = CI::$APP->config->item('minify_level');

        $this->routeURL = 'No-Route-Url';
    }

    public function __get($class)
    {
        return CI::$APP->$class;
    }

    private function _setCacheName(){
        return md5($this->session->session_id.'@'.$this->uri->uri_string());
    }

    private function _getCacheData($cache_name = ''){
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        return $this->cache->get($cache_name);
    }

    public function render_view($content = '', $data = array(), $is_cached = false, $view_file = 'template'){
        if($is_cached && $this->use_cache){
            $cache_name = $this->_setCacheName();
            $view = $this->_getCacheData();
            if (!$view) {
                $view = $this->_html_minify($this->_render_view($content, $data, $view_file));
                $this->cache->save($cache_name, $view, $this->cache_time);
            }
        } else {
            $view = $this->_html_minify($this->_render_view($content, $data, $view_file));
        }
        $this->output->set_output($view);
    }

    private function _render_view($content = '', $data = array(), $view_file){
        $data['routeURL'] = $this->routeURL;
        return $this->load->view($view_file, array_merge(['page' => $this->page], [
            'navbar' => $this->load->view('navbar', $this->data_navbar(), true),
            'sidebar' => $this->load->view('sidebar', $this->data_sidebar(), true),
            'breadcrumb' => $this->load->view('breadcrumb', ['breadcrumb' => (object)$this->breadcrumb], true),
            'content' => $this->load->view($this->module . '/' . $content, $data, true),
            'footer' => $this->load->view('footer', null, true),
            'css' => ($this->css === true) ? $this->load->view($this->module . '/' . $content . '_css', ['page' => $this->page], true) : '',
            'js' => ($this->js === true) ? $this->load->view($this->module . '/' . $content . '_js', ['page' => $this->page, 'routeURL' => $this->routeURL, 'data' => (object) $this->js_data], true) : ''
        ]), true);
    }

    private function data_navbar(){
        $this->load->model('template_model', 'template');
        $data['latest_news'] = $this->template->get_posts(0,5);
        return $data;
    }

    private function data_sidebar(){
        $date = date("N");
        switch ($date) {
            case "0":
                $txt_slug = "jadwal-minggu";
                break;
            case "1":
                $txt_slug = "jadwal-senin";
                break;
            case "2":
                $txt_slug = "jadwal-selasa";
                break;
            case "3":
                $txt_slug = "jadwal-rabu";
                break;
            case "4":
                $txt_slug = "jadwal-kamis";
                break;
            case "5":
                $txt_slug = "jadwal-jumat";
                break;
            case "6":
                $txt_slug = "jadwal-sabtu";
                break;
            default:
                $txt_slug = "jadwal-minggu";
        }
        $this->load->model('template_model', 'template');
        $data['jadwal'] = $this->template->get_jadwal($txt_slug);
        //$data['latest_news'] = $this->template->get_posts(0,5);
        $data['latest_event'] = $this->template->get_posts(3,5);
        return $data;
    }

    public function load_view($content = '', $data = array(), $is_cached = false){
        $view = '';
        if($is_cached && $this->use_cache){
            $cache_name = $this->_setCacheName();
            $view = $this->_getCacheData();
            if (!$view) {
                $view = $this->load->view($this->module.'/'.$content, array_merge(['page' => $this->page], $data), true);
                $view = $this->_html_minify($view);
                $this->cache->save($cache_name, $view, $this->cache_time);
            }
        } else {
            $view = $this->_html_minify($this->load->view($this->module.'/'.$content, array_merge(['page' => $this->page], $data), true));
        }
        $this->output->set_output($view);
    }

    public function modal_error($data = array(), $is_cached = false){
        $view = '';
        if($is_cached && $this->use_cache){
            $cache_name = $this->_setCacheName();
            $view = $this->_getCacheData();
            if (!$view) {
                $view = $this->load->view('errors/modal_error', $data, true);
                $view = $this->_html_minify($view);
                $this->cache->save($cache_name, $view, $this->cache_time);
            }
        } else {
            $view = $this->_html_minify($this->load->view('errors/modal_error', $data, true));
        }
        $this->output->set_output($view);
    }

    public function login_view($data = array(), $is_cached = true){
        if($is_cached && $this->use_cache){
            $cache_name = $this->_setCacheName();
            $view = $this->_getCacheData();
            if (!$view) {
                $view = $this->load->view('login', array_merge(	['page' => $this->page], $data), true);
                $view = $this->_html_minify($view);
                $this->cache->save($cache_name, $view, $this->cache_time);
            }
        } else {
            $view = $this->_html_minify($this->load->view('login', array_merge(['page' => $this->page], $data), true));
        }
        $this->output->set_output($view);
    }

    private function _html_minify($str_html = ''){
        if(!$this->use_minify){
            return $str_html;
        }

        switch ($this->minify_level){
            case 1 : $str_html = $this->_simple_minify($str_html); break;
            case 2 : {
                $this->load->library('CI_Minifier');
                $str_html = CI_Minifier::output($str_html);
            }break;
        }
        return $str_html;
    }

    private function _simple_minify($str_html = ''){
        ini_set("pcre.recursion_limit", "16777");

        $re = '%# Collapse whitespace everywhere but in blacklisted elements.
			(?>             # Match all whitespans other than single space.
			  [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
			| \s{2,}        # or two or more consecutive-any-whitespace.
			) # Note: The remaining regex consumes no text at all...
			(?=             # Ensure we are not in a blacklist tag.
			  [^<]*+        # Either zero or more non-"<" {normal*}
			  (?:           # Begin {(special normal*)*} construct
				<           # or a < starting a non-blacklist tag.
				(?!/?(?:textarea|pre|script)\b)
				[^<]*+      # more non-"<" {normal*}
			  )*+           # Finish "unrolling-the-loop"
			  (?:           # Begin alternation group.
				<           # Either a blacklist start tag.
				(?>textarea|pre|script)\b
			  | \z          # or end of file.
			  )             # End alternation group.
			)  # If we made it here, we are not in a blacklist tag.
			%Six';

        $new_buffer = preg_replace($re, " ", $str_html);
        return ($new_buffer === null)? $str_html : $new_buffer;
    }

    /*
     * Cek login user:
     * 		-	jika belum login langsung redirect ke halaman login
     * 		-	jika sudah login, cek apakah punya hak akses terhadap menu/controller yang di akses
     */
    protected function authCheck($hak_akses = false){
        if($this->session->userdata('is_login') !== true){
            redirect('login');
        } else {
            if(!isset($this->session->access[strtoupper($this->kodeMenu)])){
                show_404('AuthCheck');
            }
        }
    }

    /*
     * Cek status login user
     * Bernilai -> 	true: login
     * 				false: belum login
     */
    protected function loginCheck(){
        return ($this->session->userdata('is_login') === TRUE);
    }

    /*
     * Cek status akses C-R-U-D user
     * Bernilai -> 	true: memiliki akses
     * 				false: tidak memiliki akses
     */
    protected function authCheckDetailAccess($action = 'r', $is_modal = false){
        if($this->session->access[strtoupper($this->kodeMenu)][$action] != 1){
            if($is_modal){
                $this->modal_error(['data' => (object) ['header' => 'Error Akses', 'title' => 'Terjadi Kesalahan.', 'message' => 'Akses Ditolak. Halaman yang diakses tidak tersedia.']]);
                return false;
            }else{
                show_404('DetailAccess');
            }
        }
        return true;
    }

    public function set_json($data = array(), $status = 200){
        $this->output->set_status_header($status)->set_content_type('application/json')->set_output(json_encode($data, JSON_NUMERIC_CHECK ));
    }

    public function set_header($name = 'filename'){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$name.'"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
    }


    public function validateAPIRequest(){
        $this->load->model('api/auth_model', 'auth');
        $credential = $this->api_headers();
        $check = $this->auth->validateAPIToken($credential->user_id, $credential->identifier, $credential->token);
        if($check === false){
            $this->set_json(['status' => false, 'message' => $this->auth->getMessage()], 403);
        }
        return $check;
    }

    public function input_get($name = null, $xss = true){
        return $this->input->get($name, $xss);
    }

    public function input_post($name = null, $xss = true){
        return $this->input->post($name, $xss);
    }

    public function input_header($name = null, $xss = true){
        return $this->input->get_request_header($name, $xss);
    }

    public function api_headers($xss = true){
        return (object) ['user_id'    => $this->input_header($this->config->item('api_user_id'), $xss),
            'identifier' => $this->input_header($this->config->item('api_identifier'), $xss),
            'token'      => $this->input_header($this->config->item('api_token_name'), $xss),];
    }

    public function api_user($xss = true){
        return $this->input_header($this->config->item('api_user_id'), $xss);
    }

    public function api_token($xss = true){
        return $this->input_header($this->config->item('api_token_name'), $xss);
    }

    public function api_identifier($xss = true){
        return $this->input_header($this->config->item('api_identifier'), $xss);
    }

    public function get_method(){
        return $this->input->method();
    }

    protected function getCsrfName(){
        return $this->tokenName;
    }

    protected function getCsrfToken(){
        return $this->tokenHash;
    }

    protected function getPeriodDate($start, $end){
        return new DatePeriod(new DateTime($start), new DateInterval('P1D'), new DateTime($end));
    }
}