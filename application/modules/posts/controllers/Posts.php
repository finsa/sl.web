<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'posts';
		//$this->authCheck();
		
		$this->load->model('posts_model', 'model');
    }
	
	public function index($txt_posts_slug, $int_posts_id){
		$this->page->menu = 'dashboard';
        $data['posts']  = $this->model->get_posts_detail($int_posts_id);
		$this->render_view('index', $data, false);
	}

}
