<div class="col-12 col-lg-8">
    <div class="blog-posts-area">

        <!-- Single Featured Post -->
        <div class="single-blog-post featured-post single-post">
            <div class="post-thumb">
                <?php
                    //foreach ($posts->galeri as $gal){
                ?>
                <a href="#"><img src="<?=cdn_url().$posts->galeri[0]->txt_dir?>" alt=""></a>
                <?php //} ?>
            </div>
            <div class="post-data">
                <a href="#" class="post-catagory"><?=$posts->txt_posts_category?></a>
                <a href="<?=base_url().$posts->txt_posts_slug.'/'.$posts->int_posts_id?>" class="post-title">
                    <h6><?=$posts->txt_posts_title?></h6>
                </a>
                <div class="post-meta">
                    <p class="post-author">By <a href="#"><?=$posts->txt_posts_author?></a></p>
                    <?=$posts->txt_posts_content?>
                    <div class="newspaper-post-like d-flex align-items-center justify-content-between">
                        <!-- Tags -->
                        <div class="newspaper-tags d-flex">
                            <span>Tags:</span>
                            <ul class="d-flex">
                            <?php
                                foreach ($posts->posts_tag as $tag){
                            ?>
                            <li><a href="<?=base_url().'/tag/'.$tag->txt_slug?>">#<?=$tag->txt_tag?> </a></li>
                            <?php } ?>
                            </ul>
                        </div>

                        <!-- Post Like & Post Comment --
                        <div class="d-flex align-items-center post-like--comments">
                            <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                            <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

