<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Posts_model extends MY_Model {

	public function get_posts_detail($int_posts_id, $with_galeri = true){
		$data = $this->db->query("SELECT *
									FROM  {$this->t_posts} 
									WHERE int_posts_id = {$int_posts_id}")->row_array();
		if(!empty($data)){
			$posts_tag  = $this->db->query("SELECT {$this->t_posts_tag}.int_tag_id, {$this->m_tag}.txt_tag, {$this->m_tag}.txt_slug FROM {$this->t_posts_tag}
                                            LEFT JOIN {$this->m_tag} ON {$this->t_posts_tag}.int_tag_id = {$this->m_tag}.int_tag_id
                                            WHERE int_posts_id = ?", [$int_posts_id])->result();
		}

		$galeri = ($with_galeri)? $this->get_galeri($int_posts_id) : null;
		return (object) array_merge($data, ['galeri' => $galeri], ['posts_tag' => $posts_tag]);
	}

	public function get_galeri($int_posts_id){
		return $this->db->query("SELECT *
								FROM  {$this->t_posts_img} 
								WHERE int_posts_id = {$int_posts_id}
								ORDER BY is_cover DESC")->result();
			
	}

}