<div id="modal-retur" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?php echo $title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body p-0">
            <table class="table table-striped table-hover table-full-width mb-0" id="table_pembelian">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Rekening Pembayaran</th>
                    <th class="text-right">Total</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $total = 0;
                if($data) {
                    foreach ($data as $i => $d) {
                        ?>
                        <tr>
                            <td width="10%"><?php echo $i + 1 ?></td>
                            <td width="70%"><?php echo $d->rekening_nama ?></td>
                            <td width="20%" class="text-right"><?php echo number_format($d->total, 0, ',', '.') ?></td>
                        </tr>
                        <?php
                        $total += $d->total;
                    }
                }else{
                    echo '<tr><td colspan="3" class="text-center">No Data</td></tr>';
                }

            ?>
            </tbody>
            <?php if($total > 0){ ?>
                <tfoot>
                    <tr>
                        <td width="80%" colspan="2" class="text-right text-bold">Total</td>
                        <td width="20%" class="text-right"><?php echo number_format($total, 0, ',', '.')?></td>
                    </tr>
                </tfoot>
            <?php }?>
            </table>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-warning">Keluar</button>
		</div>
	</div>
</div>