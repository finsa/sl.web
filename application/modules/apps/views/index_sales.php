<div id="modal-retur" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title"><?php echo $title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body p-0">
            <table class="table table-striped table-hover table-full-width mb-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Barang</th>
                    <th class="text-right">Harga</th>
                    <th class="text-right">Qty</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $total = 0;
                $footer = '';
                if($data) {
                    $qty = 0;
                    foreach ($data as $i => $d) {
            ?>
                <tr>
                    <td width="5%"><?php echo $i + 1 ?></td>
                    <td width="65%"><?php echo $d->barang_nama ?></td>
                    <td width="15%" class="text-right"><?php echo number_format($d->harga, 0, ',', '.') ?></td>
                    <td width="15%" class="text-right"><?php echo number_format($d->qty, 0, ',', '.') ?></td>
                </tr>
            <?php
                        $qty += $d->qty;
                        $total += $d->total;
                    }

                    $footer .= '<tfoot><tr><td colspan="3" class="text-right"><b>Total:</b></td><td class="text-right">'.number_format($qty, 0, ',', '.').'</td></tr></tfoot>';
                }else{
                    echo '<tr><td colspan="4" class="text-center">No Data</td></tr>';
                }

            ?>
            </tbody>
                <?php echo $footer ?>
            </table>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-warning">Keluar</button>
		</div>
	</div>
</div>