<script>
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see the error "The Geolocation service
    // failed.", it means you probably did not give permission for the browser to
    // locate you.
    var map;
    function initMap() {

        var locations = [
        <?php
            $lat = 0; $long = 0;
            foreach($data->stasiun as $s){
                echo "['{$s->no}', '{$s->nama}', '{$s->kec}', {$s->lon}, {$s->lat}, '{$s->tgl}', '{$s->curah}'],";
                $lat = $s->lat;
                $long = $s->lon;
            }
        ?>
        ];

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?php echo $lat?>, lng: <?php echo $long?>},
            zoom: 10
        });
        setMarkers(map,locations);
    }

    function setMarkers(map,locations){
        let marker, content, infowindow;
        locations.forEach(function(item, i){
            let pos = new google.maps.LatLng(item[4], item[3]);
            marker  = new google.maps.Marker({
                map: map, title: item[0] , position: pos
            });
            map.setCenter(marker.getPosition());

            content = "<table><tr><td>No</td><td>&nbsp;:&nbsp;</td><td><strong>"+item[0]+"</strong></td></tr>"+
                            "<tr><td>Nama Stasiun</td><td>&nbsp;:&nbsp;</td><td><strong>"+item[1]+"</strong></td></tr>"+
                            "<tr><td>Kecamatan</td><td>&nbsp;:&nbsp;</td><td><strong>"+item[2]+"</strong></td></tr>"+
                            "<tr><td>Terakhir Survey</td><td>&nbsp;:&nbsp;</td><td><strong>"+item[5]+"</strong></td></tr>"+
                            "<tr><td>Curah Hujan</td><td>&nbsp;:&nbsp;</td><td><strong>"+item[6]+"</strong></td></tr></table>";

            infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker,'click', (function(marker, content, infowindow){
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map,marker);
                };
            })(marker,content,infowindow));
        })
    }

    $(document).ready(function(){
        initMap();
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7Z00inUT03ZBZfPfj0fRyKC_OeMsxeEk"></script>