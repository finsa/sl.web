<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		//$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        //$this->js_data = ['stasiun' => $this->model->get_stasiun()];
        //$data['total']   = $this->model->get_total_survey();
		$this->render_view('index', null, false, 'homepage');
	}

	public function income($mode){
        $data['title'] = 'Detail Pemasukan <strong>'.$this->mode[$mode - 1].'</strong>';
        $data['data']  = $this->report->getIncomeDetail($mode);
        $this->load_view('index_income', $data, false);
    }

    public function sales($mode){
        $data['title'] = 'Top <strong>10</strong> Detail Penjualan <strong>'.$this->mode[$mode - 1].'</strong>';
        $data['data']  = $this->report->getSalesDetail($mode, 10);
        $this->load_view('index_sales', $data, false);
    }

    public function sales_chart(){
        $data  = array();
        $ldata = $this->report->listPenjualan($this->input_post('mode'));
        $data  = [];
        $nilai = [];

        foreach ($ldata as $d){
            $data["labels"][] =  $d->tgl;
			$nilai[] = $d->total;
		}

       $data['datasets'] = array(array( "label" => "Transaksi",
                                        "data" => $nilai,
                                        "backgroundColor" => "rgba(153, 102, 255, 0.2)",
				                        "borderColor" => "rgb(153, 102, 255)",
                                        "fill" => false,
                                        "borderWidth"=>  1));

        $this->set_json(array('stat' => TRUE,
            'data' => $data,
            $this->security->get_csrf_token_name() => $this->security->get_csrf_hash()));
    }

    public function product_chart(){
        $data  = array();
        $ldata = $this->report->listProdukTerjual($this->input_post('mode'));
        $data  = [];
        $nilai = [];

        $bgc = [];
        $bc = [];
        foreach ($ldata as $d){
            $data["labels"][] =  $d->tgl;
            $nilai[] = $d->total;
        }

        $data['datasets'] = array(array( "label" => "Pcs",
            "data" => $nilai,
            "backgroundColor" => "rgba(153, 102, 255, 0.2)",
            "borderColor" => "rgb(153, 102, 255)",
            "fill" => false,
            "borderWidth"=>  1));

        $this->set_json(array('stat' => TRUE,
            'data' => $data,
            $this->security->get_csrf_token_name() => $this->security->get_csrf_hash()));
    }

    public function omset_chart(){
        $data  = array();
        $ldata = $this->report->listOmset($this->input_post('mode'));
        $data  = [];
        $nilai = [];

        foreach ($ldata as $d){
            $data["labels"][] =  $d->tgl;
            $nilai[] = $d->total;
        }

        $data['datasets'] = array(array( "label" => "rupiah",
            "data" => $nilai,
            "backgroundColor" => "rgba(153, 102, 255, 0.2)",
            "borderColor" => "rgb(153, 102, 255)",
            "fill" => false,
            "borderWidth"=>  1));

        $this->set_json(array('stat' => TRUE,
            'data' => $data,
            $this->security->get_csrf_token_name() => $this->security->get_csrf_hash()));
    }

    public function omsetavg_chart(){
        $data  = array();
        $ldata = $this->report->listOmset($this->input_post('mode'));
        $data  = [];
        $nilai = [];

        foreach ($ldata as $d){
            $data["labels"][] =  $d->tgl;
            $nilai[] = intval($d->total / (($d->jumlah > 0)? $d->jumlah : 1));
        }

        $data['datasets'] = array(array( "label" => "rupiah",
            "data" => $nilai,
            "backgroundColor" => "rgba(153, 102, 255, 0.2)",
            "borderColor" => "rgb(153, 102, 255)",
            "fill" => false,
            "borderWidth"=>  1));

        $this->set_json(array('stat' => TRUE,
            'data' => $data,
            $this->security->get_csrf_token_name() => $this->security->get_csrf_hash()));
    }
}
