<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

	public function get_total_survey(){
	    $where = $this->whereBetweenDateToDays('dtSurvey', date('Y-m-01'), date('Y-m-d'), true);
	    return $this->db->query("SELECT   (SELECT  COUNT(*) FROM {$this->t_bangunan} WHERE {$this->db->escape($where)} ) as bangunan,
                                          (SELECT  COUNT(*) FROM {$this->t_curah_hujan} WHERE {$this->db->escape($where)} ) as curah,
                                          (SELECT  COUNT(*) FROM {$this->t_kerusakan} WHERE {$this->db->escape($where)} ) as tanaman,
                                          (SELECT  COUNT(*) FROM {$this->t_pintu_air} WHERE {$this->db->escape($where)} ) as pintu,
                                          (SELECT  COUNT(*) FROM {$this->t_realisasi} WHERE {$this->db->escape($where)} ) as realisasi")->row();
    }

    public function get_stasiun(){
	    return $this->db->query("SELECT  s.txtNo as no, s.txtStasiunCurahHujan as nama, k.txtKecamatan as kec, s.dblLatitude as lat, s.dblLongitude as lon,
                                         (SELECT DATE_FORMAT(c1.dtSurvey, '%d %b %Y') FROM tr_surveycurahhujan c1 WHERE c1.intIDStasiunCurahHujan = s.intIDStasiunCurahHujan ORDER BY c1.dtSurvey DESC LIMIT 1) as tgl,
                                         (SELECT c2.dblCurahHujan FROM tr_surveycurahhujan c2 WHERE c2.intIDStasiunCurahHujan = s.intIDStasiunCurahHujan ORDER BY c2.dtSurvey DESC LIMIT 1) as curah
                                 FROM    {$this->m_stasiun} s 
                                 JOIN    {$this->r_kecamatan} k ON s.intIDKecamatan = k.intIDKecamatan")->result();
    }
}