<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Auth_model extends MY_Model {
	private $userMenu = '';
	private $userCredential = array();
	private $userLoginMessage = '';
	private $userAccessMenu = array();
	private $userRedirect = '';

	public function getAuthCredential($username, $password){
		$return = false;
		$query = $this->db->query("SELECT  u.intIDuser as user_id, u.txtNamaDepan as first_name, u.txtNamaBelakang as last_name, u.txtPassword as password, u.intStatus as status, u.intIDGroup as group_id, g.txtnama as group_name
                                   FROM    {$this->s_user} u
                                   JOIN    {$this->s_group} g ON u.IntIdGroup = g.IntIdGroup 
                                   WHERE   u.txtUsername = ?", [$username]);
		if($query->num_rows() == 1){
			$query = $query->row();

			if($query->status){
				if(password_verify($password, $query->password)){
					$this->userCredential = ['user_id' => $query->user_id,
											 'username' => $username,
											 'first_name' => $query->first_name,
											 'last_name' => $query->last_name,
                                             'group_id' => $query->group_id,
                                             'group' => $query->group_name];
					
					$this->userAccessMenu['APP-HOME'] = ['c' => 0, 'r' => 1, 'u' => 0, 'd' => 0];
					$this->_getUserMenu($query->group_id);
					$this->userLoginMessage = 'Login berhasil. Silahkan tunggu...';
					$return = true;
				} else {
					$this->userLoginMessage = 'Username dan Password salah';
				}
			} else {
				$this->userLoginMessage = 'User tidak aktif.';
			}
		} else {
			$this->userLoginMessage = 'Username dan Password salah';
		} 
		return $return;
	}
	 
	private function _getUserMenu($group_id, $parent_id = null){
		$this->db->select("m.intIdMenu as menu_id, m.txtKode as kode, m.txtNama as nama, m.txtUrl as url, m.intLevel as level, m.txtClass as class, m.txtIcon as icon, (SELECT COUNT(*) FROM {$this->s_menu} mm WHERE mm.intParentId = m.intIdMenu) as sub, gm.c, gm.r, gm.u, gm.d")
							->from($this->s_group_menu.' gm')
							->join($this->s_menu.' m', 'gm.intIdMenu = m.intIdMenu')
							->where('gm.intIdGroup', $this->binaryUUID($group_id), false)
							->where('m.isActive', 1)
							->order_by('m.intUrutan');
		if(empty($parent_id)){
			$this->db->where("(m.intParentId is null or m.intLevel = 1)");
		} else {
			$this->db->where("(m.intParentId = {$this->binaryUUID($parent_id)} and m.intLevel > 1)", null, false);
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = $query->result();
			foreach ($data as $d) {
				$this->userAccessMenu[strtoupper($d->kode)] = ['c' => $d->c, 'r' => $d->r, 'u' => $d->u, 'd' => $d->d];
				if($d->sub == 0){
					$this->userMenu .=  '<li class="nav-item">' .
										'<a href="'.(empty($d->url)? '#' : site_url($d->url)).'" class="nav-link '.$d->class.' l'.$d->level.'">' .
										'<i class="nav-icon '.$d->icon.'"></i><p>'.$d->nama.'</p></a></li>';
				} else {
					$this->userMenu .= 	'<li class="nav-item has-treeview ">' .
                    					'<a href="#" class="nav-link '.$d->class.' l'.$d->level.'">' .
										'<i class="nav-icon '.$d->icon.'"></i>' .
										'<p>'.$d->nama.'<i class="fas fa-angle-left right"></i></p></a>' .
										'<ul class="nav nav-treeview">';

					$this->_getUserMenu($group_id, $d->menu_id);
					$this->userMenu .= '</ul>';
				}
				
				if(empty($this->userRedirect)){
					$this->userRedirect = site_url($d->url);
				}
			}
		}
	}

	public function getLoginMessage(){
		return $this->userLoginMessage;
	}

	public function getUserCredential(){
		return $this->userCredential;
	}

	public function getUserMenu(){
		return $this->userMenu;
	}

	public function getUserAccessMenu(){
		return $this->userAccessMenu;
	}
	
	public function getUserRedirect(){
		return $this->userRedirect;
	}
	
	public function	_getUserAccessMenu($group_id, $kode){
		return $this->db->query("	SELECT  c,r,u,d 
									FROM    {$this->s_group_menu} gm 
									JOIN    {$this->s_menu} m ON gm.menu_id = m.menu_id
									WHERE   gm.group_id = {$this->binaryUUID($group_id)} AND m.kode = ?", [$kode])->row();
	}
}