<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		//$this->authCheck();
		
		$this->load->model('homepage_model', 'model');
    }
	
	public function index(){
		$date = date("N");
        switch ($date) {
            case "0":
                $txt_slug = "jadwal-minggu";
                break;
            case "1":
                $txt_slug = "jadwal-senin";
                break;
            case "2":
                $txt_slug = "jadwal-selasa";
                break;
            case "3":
                $txt_slug = "jadwal-rabu";
                break;
            case "4":
                $txt_slug = "jadwal-kamis";
                break;
            case "5":
                $txt_slug = "jadwal-jumat";
                break;
            case "6":
                $txt_slug = "jadwal-sabtu";
                break;
            default:
                $txt_slug = "jadwal-minggu";
		}
        $data['news']  = $this->model->get_posts(0,3);
        $data['article']   = $this->model->get_posts(2,6);
        $data['event']   = $this->model->get_posts(3,5);
        $data['jadwal']   = $this->model->get_jadwal($txt_slug);
		$this->render_view('index', $data, false, 'homepage');
	}
}
