    <!-- ##### Featured Post Area Start ##### -->
    <?//=print_r($jadwal)?>
    <div class="featured-post-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="row">
                        <!-- Single Featured Post -->
                        <div class="col-12 col-lg-7">
                            <?php if(!empty($news[0])){ ?>
                                <div class="single-blog-post featured-post">
                                    <div class="post-thumb">
                                        <a href="#"><img src="<?=cdn_url().$news[0]->txt_dir?>" alt=""></a>
                                    </div>
                                    <div class="post-data">
                                        <a href="<?=base_url().'/cat/'.$news[0]->int_posts_category?>#" class="post-catagory"><?=$news[0]->txt_posts_category?></a>
                                        <a href="<?=base_url().$news[0]->txt_posts_slug.'/'.$news[0]->int_posts_id?>" class="post-title">
                                            <h6><?=$news[0]->txt_posts_title?></h6>
                                        </a>
                                        <div class="post-meta">
                                            <p class="post-author">Oleh. <a href="#"><?=$news[0]->txt_posts_author?></a></p>
                                            <div class="post-excerp"><?=substr($news[0]->txt_posts_content,0,strpos($news[0]->txt_posts_content,' ',250))?>...</div>
                                            <!-- Post Like & Post Comment 
                                            <div class="d-flex align-items-center">
                                                <a href="#" class="post-like"><img src="<?=base_url()?>theme/img/core-img/like.png" alt=""> <span>392</span></a>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="col-12 col-lg-5">
                            <!-- Single Featured Post -->
                            <?php for ($i = 1; $i < 3; $i++){
                            if(!empty($news[$i])){ ?>
                            <div class="single-blog-post featured-post-2">
                                <div class="post-thumb">
                                    <a href="#"><img src="<?=cdn_url().$news[$i]->txt_dir?>" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="<?=base_url().'/cat/'.$news[$i]->int_posts_category?>" class="post-catagory"><?=$news[$i]->txt_posts_category?></a>
                                    <div class="post-meta">
                                        <a href="<?=base_url().$news[$i]->txt_posts_slug.'/'.$news[$i]->int_posts_id?>" class="post-title">
                                            <h6><?=$news[$i]->txt_posts_title?></h6>
                                        </a>
                                        <!-- Post Like & Post Comment 
                                        <div class="d-flex align-items-center">
                                            <a href="#" class="post-like"><img src="<?=base_url()?>theme/img/core-img/like.png" alt=""> <span>392</span></a>
                                            <a href="#" class="post-comment"><img src="<?=base_url()?>theme/img/core-img/chat.png" alt=""> <span>10</span></a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <?php } } ?>
                        </div>
                    </div>

                    <div class="section-heading">
                        <h6>Artikel</h6>
                    </div>
                    
                    <div class="row">
                        <!-- Single Post -->
                        <?php foreach ($article as $ar){?>
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <img src="<?=cdn_url().$ar->txt_dir?>" alt="" class="article-thumb">
                                </div>
                                <div class="post-data">
                                    <a href="<?=base_url().'/cat/'.$ar->int_posts_category?>" class="post-catagory"><?=$ar->txt_posts_category?></a>
                                    <a href="<?=base_url().$ar->txt_posts_slug.'/'.$ar->int_posts_id?>" class="post-title">
                                        <h6><?=$ar->txt_posts_title?></h6>
                                    </a>
                                    <!--<div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="<?=base_url()?>theme/img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="<?=base_url()?>theme/img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Single Featured Post -->
                    <div class="section-heading" style="margin-bottom:0px">
                        <h6>SL Hari Ini</h6>
                    </div>
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-data">
                            <div class="post-meta">
                                <div><?=$jadwal[0]->txt_content?></div>
                            </div>
                        </div>
                    </div>

                    <div class="section-heading" style="margin-top:50px">
                        <h6>Even</h6>
                    </div>
                    <!-- Popular News Widget -->
                    <div class="popular-news-widget mb-30">
                        <?php $n=0; foreach ($event as $ev){ $n++;?>
                        <div class="single-popular-post">
                            <a href="<?=base_url().$ev->txt_posts_slug.'/'.$ev->int_posts_id?>">
                                <h6><span><?=$n?>.</span> <?=$ev->txt_posts_title?></h6>
                            </a>
                            <p><?=$ev->dt_publish?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Video Post Area Start ##### --
    <div class="video-post-area bg-img bg-overlay" style="background-image: url(img/bg-img/bg1.jpg);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?=base_url()?>theme/img/bg-img/video1.jpg" alt="">
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?=base_url()?>theme/img/bg-img/video2.jpg" alt="">
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-video-post">
                        <img src="<?=base_url()?>theme/img/bg-img/video3.jpg" alt="">
                        <div class="videobtn">
                            <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <-- ##### Video Post Area End ##### -->
