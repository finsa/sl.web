<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation 
{
    public $CI;

    /*function is_unique($str, $field){
        list($table, $field) = explode('.', $field);
        return isset($this->CI->db)
			? ($this->CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 0)
            : FALSE;
    }*/

    public function is_exists($str, $field){
        sscanf($field, '%[^.].%[^.]', $table, $field);
        $this->CI->form_validation->set_message('is_exists', "Input data {$str} tidak terdaftar pada sistem.");
        return is_object($this->CI->db)
            ? ($this->CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() > 0)
            : FALSE;
    }

    public function is_exists_uuid($str, $field){
        sscanf($field, '%[^.].%[^.]', $table, $field);
        $this->CI->form_validation->set_message('is_exists_uuid', "Input data {$str} tidak terdaftar pada sistem.");

        $str = ((substr($str, 0,2) == '0x')? preg_replace("/[^A-Fa-f0-9x]/", '', $str) : '0x'.preg_replace("/[^A-Fa-f0-9]/", '', $str));
        return is_object($this->CI->db)
            ? ($this->CI->db->from($table)->where("`{$field}` = {$str} ", null, false)->count_all_results() > 0)
            : FALSE;
    }

    public function is_unique($str, $field){
        sscanf($field, '%[^.].%[^.]', $table, $field);
        //return isset($this->CI->db)
        return is_object($this->CI->db)
            ? ($this->CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 0)
            : FALSE;
    }

    public function is_unique_update($kode, $field){
        list($table, $field, $id, $val) = explode('.', $field);

        $val = preg_replace("/[^0-9]/", '', $val);
        $this->CI->form_validation->set_message('is_unique_update', "Input data {$kode} sudah ada dalam sistem.");
        if (is_object($this->CI->db)){
            $check = $this->CI->db->query("SELECT COUNT(*) as total FROM {$table} WHERE {$field} = ? AND {$id} != ? ",[$kode, $val])->row();
            return ($check->total == 0);
        } else {
            return FALSE;
        }
    }

    public function validDateTime($str){
        $this->CI->form_validation->set_message('validDateTime', "Input tanggal {$str} tidak valid. Format yang dibutuhkan Y-m-d H:i:s");
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $str);
        return $d && $d->format('Y-m-d H:i:s') == $str;
    }

    public function validDate($str){
        $this->CI->form_validation->set_message('validDate', "Input tanggal {$str} tidak valid. Format yang dibutuhkan Y-m-d");
        $d = DateTime::createFromFormat('Y-m-d', $str);
        return $d && $d->format('Y-m-d') == $str;
    }

    public function validTime($str){
        $this->CI->form_validation->set_message('validTime', "Input jam {$str} tidak valid. Format yang dibutuhkan H:i");
        $d = DateTime::createFromFormat('H:i', $str);
        return $d && $d->format('H:i') == $str;
    }

    public function validUUID($guid){
        $this->CI->form_validation->set_message('validUUID', 'Input data harus bertipe UUID');
        return ((ctype_xdigit($guid) && strlen($guid) == 32) OR strlen($guid) == 0);
    }
}