<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class MY_Model extends CI_Model {
	private $query_log = array();

	public $s_surveyor 		= 'm_user';
	public $s_user 		    = 'm_user_admin';
    public $s_group 		= 'm_user_admin_group';
    public $s_menu			= 'm_user_admin_menu';
    public $s_group_menu	= 'm_user_admin_group_menu';
    
    public $t_pages         = 't_pages';
    public $t_pages_img     = 't_pages_img';

    public $t_posts         = 't_posts';
    public $t_posts_img     = 't_posts_img';
    public $t_posts_tag     = 't_posts_tag';

    public $m_category	    = 'm_category';
    public $m_tag		    = 'm_tag';
    public $m_author	    = 'm_author';
    public $m_tourism	    = 'm_tourism';
    public $m_tourism_img	= 'm_tourism_img';
    public $m_tourism_cat   = 'm_tourism_category';

    public $schema          = 'cmstb';


    public function setQueryLog($title = null){
		$this->query_log[$title] = $this->db->last_query();
	}
	
	public function getQueryLog($title = null){
		return empty($title)? $this->query_log : (isset($this->query_log[$title])? $this->query_log[$title] : "Query Log '{$title}' not found.");
	}

    public function setProcedureLog($log = null){
        $this->query_log['sp'][] = $log;
    }

    public function getProcedureLog(){
        return $this->query_log['sp'];
    }

	public function callSimpleProcedure($sp){
        $this->db->simple_query("CALL {$this->schema}.{$sp}");
        $this->setProcedureLog("CALL {$this->schema}.{$sp}");
    }

	public function callProcedure($sp, $escape_params = [], $return_as = 'row'){
	    $query_sp = $this->db->query("CALL {$this->schema}.{$sp} ", $escape_params);
        $this->setProcedureLog("CALL {$this->schema}.{$sp} ");
	    $res = null;
	    switch (strtolower($return_as)){
            case 'row': $res = $query_sp->row(); break;
            case 'result': $res = $query_sp->result(); break;
        }

        mysqli_next_result( $this->db->conn_id );
        $query_sp->free_result();
        return $res;
    }

	/* Hack way to solve "Commands out of sync; you can't run this command now" while use multiple Call Store Procedure */
	public function clearQuery($query){
        mysqli_next_result( $this->db->conn_id );
        $query->free_result();
    }

	//Menghapus elemen array saat mau insert
	public function clearFormInsert($input, $remove = array()){
		if(count($remove) > 0){
			foreach($remove as $r){
				if(isset($input[$r])){
					unset($input[$r]);
				}
			} 
		}
		return $input;
	}

	public function inputMoney($str){
	    $str = str_replace(',', '.', $str);
	    return floatval(preg_replace("/[^0-9.]/", '', $str));
    }

    public function inputNumber($str){
        return intval(preg_replace("/[^0-9]/", '', $str));
    }

    public function filterUUID($str){
        return preg_replace("/[^A-Fa-f0-9]/", '', $str);
    }

    public function isValidUUID($guid){
        return (ctype_xdigit($guid) && strlen($guid) == 32);
    }

    // Generate Hex UUID (without dash) with prefix 0x for insert Database a binary
    public function getBinaryUUID(){
	    return '0x'.Uuid::uuid1()->getHex();
    }

	// Generate Hex UUID (without dash)
    public function getHexUUID(){
        return Uuid::uuid1()->getHex();
    }

	// Convert (add prefix) Hex UUID into binary, using prefix 0x
    public function binaryUUID($str){
        return ((substr($str, 0,2) == '0x')? preg_replace("/[^A-Fa-f0-9x]/", '', $str) : '0x'.preg_replace("/[^A-Fa-f0-9]/", '', $str));
    }

    public function filterAlphaNumeric($str = ''){
        return preg_replace("/[^A-Za-z0-9 _-]/", '', $str);
    }

    /*  set data untuk INSERT/UPDATE dimana tanpa ada escape.
        data diambil dari form input
    */
    public function inputSetUUID($in, $col){
		if(isset($in[$col])){
			if(empty($in[$col])){
				$this->db->set($col, null);
			}else{
			    $this->db->set($col, $this->binaryUUID($in[$col]), false);
			}
		}
	    unset($in[$col]);
	    return $in;
    }

    /*  set data untuk INSERT/UPDATE dimana tanpa ada escape.
        data di create untuk data baru.
        return hex UUID (bukan binary UUID)
    */
    public function inputCreateUUID($col){
        $uuid = $this->getHexUUID();
        $this->db->set($col, $this->binaryUUID($uuid), false);
        return $uuid;
    }

    /*  set SQL where untuk UUID tanpa ada escape.
        data di dapat dari paremeter
    */
    public function setWhereUUID($col, $hexUUID){
        $this->db->where($col, $this->binaryUUID($hexUUID), false);
        //$this->db->where("{$col} = {$this->binaryUUID($hexUUID)}", null, false);
    }

    public function whereBetweenDate($column, $start, $end, $return_as_string = false){
        if(! validateDate($start)){
            $start = convertValidDate($start, 'Y-m-d', 'Y-m-d 00:00:00');
        }

        if(! validateDate($end)){
            $end   = convertValidDate($end, 'Y-m-d', 'Y-m-d 23:59:59');
        }
        if($return_as_string){
            return "( {$column} BETWEEN {$this->db->escape($start)} AND {$this->db->escape($end)} )";
        }else{
            $this->db->where("( {$column} BETWEEN {$this->db->escape($start)} AND {$this->db->escape($end)} )");
        }
    }

    public function whereBetweenDateSingle($column, $date, $return_as_string = false){
        $date  = convertValidDate($date, 'Y-m-d', 'Y-m-d');
        $start = $date.' 00:00:00';
        $end   = $date.' 23:59:59';
        if($return_as_string){
            return "( {$column} BETWEEN {$this->db->escape($start)} AND {$this->db->escape($end)} )";
        }else{
            $this->db->where("( {$column} BETWEEN {$this->db->escape($start)} AND {$this->db->escape($end)} )");
        }
    }

    public function whereBetweenDateToDays($column, $start, $end, $return_as_string = false){
        if($return_as_string){
            return "( {$column} BETWEEN TO_DAYS({$this->db->escape($start)}) AND TO_DAYS({$this->db->escape($end)}) )";
        }else{
            $this->db->where("( {$column} BETWEEN TO_DAYS({$this->db->escape($start)}) AND TO_DAYS({$this->db->escape($end)}) )");
        }
    }

    public function Stock_Update(){
        $this->callSimpleProcedure("sp_Stock_Update({$this->db->escape($this->session->username)})");
    }

    public function Stock_UpdateByStockOpname($opname_id){
		$this->callSimpleProcedure("sp_Stock_UpdateByStockOpname({$this->binaryUUID($opname_id)}, {$this->db->escape($this->session->username)})");
    }

    public function getNextKode($prefix = 'KODE', $table = 'table', $column = 'column', $len = 5, $separator = '-', $_where = array()){
        $this->db->select($column)->from($table)->like($column, $prefix);
        if(!empty($_where)){ $this->db->where($_where); }
        $data = $this->db->order_by($column, 'DESC')->limit(1)->get()->row();
        return empty($data)? $prefix.$separator.str_pad('1', $len, '0', STR_PAD_LEFT) : $prefix.$separator.str_pad($this->inputNumber(substr($data->$column, -$len)) + 1, $len, '0', STR_PAD_LEFT);
    }

    public function mappingTableAttributes($maps, $in){
        $data = [];
        foreach($in as $key => $val){
            $data[$maps[$key]] = empty($val)? null : $val;
        }
        return $data;
    }
}