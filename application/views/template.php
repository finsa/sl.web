<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo base_url()?>theme/img/icon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="<?php echo base_url() ?>theme/css/style.css">
</head>

<body>
    <?php echo $navbar ?>

    <div class="blog-area section-padding-0-80">
        <div class="container">
            <div class="row">
                <?php echo $content ?>
                <?php echo $sidebar ?>
            </div>
        </div>
    </div>
    
    <?php echo $footer ?>

    <!-- ##### All Javascript Files ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url() ?>theme/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url() ?>theme/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url() ?>theme/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url() ?>theme/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url() ?>theme/js/active.js"></script>
</body>

</html>