    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-between">
                            <!-- Logo -->
                            <div class="logo logo-desktop">
                                <a href="<?=base_url()?>"><img src="<?=base_url()?>theme/img/logo-full.png" alt=""></a>
                            </div>

                            <!-- Login Search Area -->
                            <div class="login-search-area d-flex align-items-center">
                                <!-- Search Form --
                                <div class="search-form">
                                    <form action="#" method="post">
                                        <input type="search" name="search" class="form-control" placeholder="Search">
                                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </form>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="newspaper-main-menu" id="stickyMenu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="newspaperNav">

                        <!-- Logo -->
                        <div class="logo logo-mobile">
                            <a href="<?=base_url()?>"><img src="<?=base_url()?>theme/img/logo-min.png" alt=""></a>
                        </div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li class="active"><a href="<?=base_url()?>">Beranda</a></li>
                                    <li class="active"><a href="<?=base_url()?>profil">Profil</a></li>
                                    <li class="active"><a href="#">Jadwal</a>
                                        <ul class="dropdown">
                                            <li><a href="jadwal-senin">Senin</a></li>
                                            <li><a href="jadwal-selasa">Selasa</a></li>
                                            <li><a href="jadwal-rabu">Rabu</a></li>
                                            <li><a href="jadwal-kamis">Kamis</a></li>
                                            <li><a href="jadwal-jumat">Jumat</a></li>
                                            <li><a href="jadwal-sabtu">Sabtu</a></li>
                                            <li><a href="jadwal-minggu">Minggu</a></li>
                                        </ul>
                                    </li>
                                    <li class="active"><a href="<?=base_url()?>berita">Berita</a></li>
                                    <li class="active"><a href="<?=base_url()?>even">Even</a></li>
                                    <li class="active"><a href="<?=base_url()?>artikel">Artikel</a></li>
                                    <li class="active"><a href="<?=base_url()?>kontak">Kontak</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <!-- Breaking News Widget -->
                    <div class="breaking-news-area d-flex align-items-center">
                        <div class="news-title">
                            <p>Berita Terkini</p>
                        </div>
                        <div id="breakingNewsTicker" class="ticker">
                            <ul>
                            <?php
                                foreach ($latest_news as $n){
                                    echo '<li><a href="'.base_url().$n->txt_posts_slug.'/'.$n->int_posts_id.'">'.$n->txt_posts_title.'</a></li>';
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Hero Add --
                <div class="col-12 col-lg-4">
                    <div class="hero-add">
                        <a href="#"><img src="<?=base_url()?>theme/img/bg-img/hero-add.gif" alt=""></a>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->