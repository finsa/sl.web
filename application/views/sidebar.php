<div class="col-12 col-lg-4">
    <div class="blog-sidebar-area">
        <div class="section-heading" style="margin-bottom:0px">
            <h6>SL Hari Ini</h6>
        </div>
        <div class="single-blog-post small-featured-post d-flex">
            <div class="post-data">
                <div class="post-meta">
                    <div><?=$jadwal[0]->txt_content?></div>
                </div>
            </div>
        </div>
        <!-- Popular News Widget -->
        <div class="popular-news-widget mb-50">
            <h3>Even Terbaru</h3>

            <?php $n=0; foreach ($latest_event as $event){ $n++;?>
            <div class="single-popular-post">
                <a href="<?=base_url().$event->txt_posts_slug.'/'.$event->int_posts_id?>">
                    <h6><span><?=$n?>. </span><?=$event->txt_posts_title?></h6>
                </a>
                <p><?=date('d F Y H:i', strtotime($event->dt_publish))?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>